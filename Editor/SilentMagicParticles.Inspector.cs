using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering;
using Object = UnityEngine.Object;
using System.Linq;

// Parts of this file are based on https://github.com/Microsoft/MixedRealityToolkit-Unity/
// 	 Copyright (c) Microsoft Corporation. All rights reserved.
// 	 Licensed under the MIT License.

namespace SilentMagicParticles.Unity
{
	public partial class Inspector : ShaderGUI
	{
		public class SMPBoot : AssetPostprocessor {
			private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets,
			string[] movedFromAssetPaths) {
			var isUpdated = importedAssets.Any(path => path.StartsWith("Assets/")) &&
							importedAssets.Any(path => path.Contains("SMP_InspectorData"));

			if (isUpdated) {
				InitializeOnLoad();
			}
			}

			[InitializeOnLoadMethod]
			private static void InitializeOnLoad() {
			Inspector.LoadInspectorData();
			}
		}

        public enum DepthWrite
        {
            Off,
            On
        }

        public enum RenderingMode
        {
            Opaque = 0,
            Cutout = 1,
            Fade = 2,
            Transparent = 3,
            Additive = 4,
            Custom = 5
        }

        public enum CustomRenderingMode
        {
            Opaque = 0,
            Cutout = 1,
            Fade = 2
        }

        protected static class BaseStyles
        {
            public static string renderTypeName = "RenderType";
            public static string renderingModeName = "_Mode";
            public static string customRenderingModeName = "_CustomMode";
            public static string sourceBlendName = "_ParticleSrcBlend";
            public static string destinationBlendName = "_ParticleDstBlend";
            public static string blendOperationName = "_BlendOp";
            public static string depthTestName = "_ZTest";
            public static string depthWriteName = "_ParticleZWrite";
            public static string colorWriteMaskName = "_ColorMask";

            public static string cullModeName = "_CullMode";
            public static string renderQueueOverrideName = "_RenderQueueOverride";

            public static string alphaToMaskName = "_AtoCMode";
            public static string alphaTestOnName = "_ALPHATEST_ON";
            public static string alphaBlendOnName = "_ALPHABLEND_ON";
            public static string alphaPremultiplyOnName = "_ALPHAPREMULTIPLY_ON";

            public static readonly string[] renderingModeNames = Enum.GetNames(typeof(RenderingMode));
            public static readonly string[] customRenderingModeNames = Enum.GetNames(typeof(CustomRenderingMode));
            public static readonly string[] depthWriteNames = Enum.GetNames(typeof(DepthWrite));

            public static string stencilComparisonName = "_StencilComp";
            public static string stencilOperationName = "_StencilOp";
            public static string stencilFailName = "_StencilFail";
            public static string stencilZFailName = "_StencilZFail";
        }

		static private TextAsset inspectorData;
		public static Dictionary<string, GUIContent> styles = new Dictionary<string, GUIContent>();

		protected MaterialProperty renderQueueOverride;

		protected GUIContent Content(string i)
		{
			GUIContent style;
			if (styles.TryGetValue(i, out style))
			{
				return style;
			} 
			return new GUIContent(i);
		}

		protected Material target;
		protected MaterialEditor editor;
		Dictionary<string, MaterialProperty> props = new Dictionary<string, MaterialProperty>();

		protected MaterialProperty Property(string i)
		{
			MaterialProperty prop;
			if (props.TryGetValue(i, out prop))
			{
				return prop;
			} 
			return new MaterialProperty();
		}

		public static void LoadInspectorData()
		{
			char[] recordSep = new char[] {'\n'};
			char[] fieldSep = new char[] {'\t'};
			//if (styles.Count == 0)
			{
					string[] guids = AssetDatabase.FindAssets("t:TextAsset SMP_InspectorData." + Application.systemLanguage);
					if (guids.Length == 0)
					{
						guids = AssetDatabase.FindAssets("t:TextAsset SMP_InspectorData.English");
					}
					inspectorData = (TextAsset)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guids[0]), typeof(TextAsset));

				string[] records = inspectorData.text.Split(recordSep, System.StringSplitOptions.RemoveEmptyEntries);
				foreach (string record in records)
				{
					string[] fields = record.Split(fieldSep, 3, System.StringSplitOptions.None); 
					if (fields.Length != 3) {Debug.LogWarning("Field " + fields[0] + " only has " + fields.Length + " fields!");};
					if (fields[0] != null) styles[fields[0]] = new GUIContent(fields[1], fields[2]);  
					
				}	
			}		
		}

		protected void FindProperties(MaterialProperty[] matProps, Material material)
		{ 	
			foreach (MaterialProperty prop in matProps)
			{
				props[prop.name] = FindProperty(prop.name, matProps, false);
			}
			renderQueueOverride = props[BaseStyles.renderQueueOverrideName];
		}
        
        protected bool initialised;

        protected void Initialise(Material material)
        {
            if (!initialised)
            {
                MaterialChanged(material);
                initialised = true;
            }
        }

        protected virtual void MaterialChanged(Material material)
        {
            SetupMaterialWithRenderingMode(material, 
                (RenderingMode)props[BaseStyles.renderingModeName].floatValue, 
                (CustomRenderingMode)props[BaseStyles.customRenderingModeName].floatValue, 
                (int)Property(BaseStyles.renderQueueOverrideName).floatValue);
        }


// https://github.com/Unity-Technologies/UnityCsReference/blob/61f92bd79ae862c4465d35270f9d1d57befd1761/Editor/Mono/Inspector/MaterialEditor.cs#L1468
		public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] matProps)
		{ 
			this.target = materialEditor.target as Material;
			this.editor = materialEditor;
			Material material = this.target;
		
            FindProperties(matProps,material);

            RenderingModeOptions(materialEditor);

            DrawSectionHeaderArea(Content("s_mainOptions"));

			int propertyIndex = 0;
			foreach (MaterialProperty prop in matProps)
			{
				if (!ShaderUtil.IsShaderPropertyHidden(material.shader, propertyIndex)) 
				{
	                if (!styles.ContainsKey(prop.name) ) 
	                {
	                editor.ShaderProperty(prop, prop.displayName);
	                } else {
					ShaderProperty(prop.name);
	                }
				}
				propertyIndex++;
			}
			FooterOptions();
        }

        protected void RenderingModeOptions(MaterialEditor materialEditor)
        {
            EditorGUI.BeginChangeCheck();

            MaterialProperty renderingMode = Property(BaseStyles.renderingModeName);
            EditorGUI.showMixedValue = renderingMode.hasMixedValue;
            RenderingMode mode = (RenderingMode)renderingMode.floatValue;
            EditorGUI.BeginChangeCheck();
            mode = (RenderingMode)EditorGUILayout.Popup(renderingMode.displayName, (int)mode, BaseStyles.renderingModeNames);

            if (EditorGUI.EndChangeCheck())
            {
                materialEditor.RegisterPropertyChangeUndo(renderingMode.displayName);
                renderingMode.floatValue = (float)mode;
            }

            EditorGUI.showMixedValue = false;

            if (EditorGUI.EndChangeCheck())
            {
                Object[] targets = renderingMode.targets;

                foreach (Object target in targets)
                {
                    MaterialChanged((Material)target);
                }
            }

            if ((RenderingMode)renderingMode.floatValue == RenderingMode.Custom)
            {
                EditorGUI.indentLevel += 2;
                //customRenderingMode.floatValue = EditorGUILayout.Popup(customRenderingMode.displayName, (int)customRenderingMode.floatValue, BaseStyles.customRenderingModeNames);
                WithMaterialPropertyDropdown(Property(BaseStyles.customRenderingModeName), Enum.GetNames(typeof(CustomRenderingMode)), editor);
                ShaderProperty(BaseStyles.sourceBlendName);
                ShaderProperty(BaseStyles.destinationBlendName);
                ShaderProperty(BaseStyles.blendOperationName);
                ShaderProperty(BaseStyles.depthTestName);
                //depthWrite.floatValue = EditorGUILayout.Popup(depthWrite.displayName, (int)depthWrite.floatValue, BaseStyles.depthWriteNames);
                WithMaterialPropertyDropdown(Property(BaseStyles.depthWriteName), Enum.GetNames(typeof(DepthWrite)), editor);
                ShaderProperty(BaseStyles.colorWriteMaskName);
                EditorGUI.indentLevel -= 2;
            }

            //ShaderProperty(BaseStyles.cullModeName);
            WithMaterialPropertyDropdown(Property(BaseStyles.cullModeName), Enum.GetNames(typeof(UnityEngine.Rendering.CullMode)), editor);
        }

        protected static void SetupMaterialWithRenderingMode(Material material, RenderingMode mode, CustomRenderingMode customMode, int renderQueueOverride)
        {
            // If we aren't switching to Custom, then set default values for all RenderingMode types. Otherwise keep whatever user had before
            if (mode != RenderingMode.Custom)
            {
                material.SetInt(BaseStyles.blendOperationName, (int)BlendOp.Add);
                material.SetInt(BaseStyles.depthTestName, (int)CompareFunction.LessEqual);
                //material.SetFloat(BaseStyles.depthOffsetFactorName, 0.0f);
                //material.SetFloat(BaseStyles.depthOffsetUnitsName, 0.0f);
                material.SetInt(BaseStyles.colorWriteMaskName, (int)ColorWriteMask.All);
            }

            switch (mode)
            {
                case RenderingMode.Opaque:
                    {
                        material.SetOverrideTag(BaseStyles.renderTypeName, BaseStyles.renderingModeNames[(int)RenderingMode.Opaque]);
                        material.SetInt(BaseStyles.customRenderingModeName, (int)CustomRenderingMode.Opaque);
                        material.SetInt(BaseStyles.sourceBlendName, (int)BlendMode.One);
                        material.SetInt(BaseStyles.destinationBlendName, (int)BlendMode.Zero);
                        material.SetInt(BaseStyles.blendOperationName, (int)BlendOp.Add);
                        material.SetInt(BaseStyles.depthTestName, (int)CompareFunction.LessEqual);
                        material.SetInt(BaseStyles.depthWriteName, (int)DepthWrite.On);
                        material.SetInt(BaseStyles.alphaToMaskName, (int)DepthWrite.Off);
                        material.SetInt(BaseStyles.colorWriteMaskName, (int)ColorWriteMask.All);
                        material.DisableKeyword(BaseStyles.alphaTestOnName);
                        material.DisableKeyword(BaseStyles.alphaBlendOnName);
                        material.DisableKeyword(BaseStyles.alphaPremultiplyOnName);
                        material.renderQueue = (renderQueueOverride >= 0) ? renderQueueOverride : (int)RenderQueue.Geometry;
                    }
                    break;

                case RenderingMode.Cutout:
                    {
                        material.SetOverrideTag(BaseStyles.renderTypeName, BaseStyles.renderingModeNames[(int)RenderingMode.Cutout]);
                        material.SetInt(BaseStyles.customRenderingModeName, (int)CustomRenderingMode.Cutout);
                        material.SetInt(BaseStyles.sourceBlendName, (int)BlendMode.One);
                        material.SetInt(BaseStyles.destinationBlendName, (int)BlendMode.Zero);
                        material.SetInt(BaseStyles.blendOperationName, (int)BlendOp.Add);
                        material.SetInt(BaseStyles.depthTestName, (int)CompareFunction.LessEqual);
                        material.SetInt(BaseStyles.depthWriteName, (int)DepthWrite.On);
                        material.SetInt(BaseStyles.alphaToMaskName, (int)DepthWrite.On);
                        material.SetInt(BaseStyles.colorWriteMaskName, (int)ColorWriteMask.All);
                        material.EnableKeyword(BaseStyles.alphaTestOnName);
                        material.DisableKeyword(BaseStyles.alphaBlendOnName);
                        material.DisableKeyword(BaseStyles.alphaPremultiplyOnName);
                        material.renderQueue = (renderQueueOverride >= 0) ? renderQueueOverride : (int)RenderQueue.AlphaTest;
                    }
                    break;

                case RenderingMode.Fade:
                    {
                        material.SetOverrideTag(BaseStyles.renderTypeName, BaseStyles.renderingModeNames[(int)RenderingMode.Fade]);
                        material.SetInt(BaseStyles.customRenderingModeName, (int)CustomRenderingMode.Fade);
                        material.SetInt(BaseStyles.sourceBlendName, (int)BlendMode.SrcAlpha);
                        material.SetInt(BaseStyles.destinationBlendName, (int)BlendMode.OneMinusSrcAlpha);
                        material.SetInt(BaseStyles.blendOperationName, (int)BlendOp.Add);
                        material.SetInt(BaseStyles.depthTestName, (int)CompareFunction.LessEqual);
                        material.SetInt(BaseStyles.depthWriteName, (int)DepthWrite.Off);
                        material.SetInt(BaseStyles.alphaToMaskName, (int)DepthWrite.Off);
                        material.SetInt(BaseStyles.colorWriteMaskName, (int)ColorWriteMask.All);
                        material.DisableKeyword(BaseStyles.alphaTestOnName);
                        material.EnableKeyword(BaseStyles.alphaBlendOnName);
                        material.DisableKeyword(BaseStyles.alphaPremultiplyOnName);
                        material.renderQueue = (renderQueueOverride >= 0) ? renderQueueOverride : (int)RenderQueue.Transparent;
                    }
                    break;

                case RenderingMode.Transparent:
                    {
                        material.SetOverrideTag(BaseStyles.renderTypeName, BaseStyles.renderingModeNames[(int)RenderingMode.Fade]);
                        material.SetInt(BaseStyles.customRenderingModeName, (int)CustomRenderingMode.Fade);
                        material.SetInt(BaseStyles.sourceBlendName, (int)BlendMode.One);
                        material.SetInt(BaseStyles.destinationBlendName, (int)BlendMode.OneMinusSrcAlpha);
                        material.SetInt(BaseStyles.blendOperationName, (int)BlendOp.Add);
                        material.SetInt(BaseStyles.depthTestName, (int)CompareFunction.LessEqual);
                        material.SetInt(BaseStyles.depthWriteName, (int)DepthWrite.Off);
                        material.SetInt(BaseStyles.alphaToMaskName, (int)DepthWrite.Off);
                        material.SetInt(BaseStyles.colorWriteMaskName, (int)ColorWriteMask.All);
                        material.DisableKeyword(BaseStyles.alphaTestOnName);
                        material.EnableKeyword(BaseStyles.alphaBlendOnName);
                        material.DisableKeyword(BaseStyles.alphaPremultiplyOnName);
                        material.renderQueue = (renderQueueOverride >= 0) ? renderQueueOverride : (int)RenderQueue.Transparent;
                    }
                    break;

                case RenderingMode.Additive:
                    {
                        material.SetOverrideTag(BaseStyles.renderTypeName, BaseStyles.renderingModeNames[(int)RenderingMode.Fade]);
                        material.SetInt(BaseStyles.customRenderingModeName, (int)CustomRenderingMode.Fade);
                        material.SetInt(BaseStyles.sourceBlendName, (int)BlendMode.One);
                        material.SetInt(BaseStyles.destinationBlendName, (int)BlendMode.One);
                        material.SetInt(BaseStyles.blendOperationName, (int)BlendOp.Add);
                        material.SetInt(BaseStyles.depthTestName, (int)CompareFunction.LessEqual);
                        material.SetInt(BaseStyles.depthWriteName, (int)DepthWrite.Off);
                        material.SetInt(BaseStyles.alphaToMaskName, (int)DepthWrite.Off);
                        material.SetInt(BaseStyles.colorWriteMaskName, (int)ColorWriteMask.All);
                        material.DisableKeyword(BaseStyles.alphaTestOnName);
                        material.EnableKeyword(BaseStyles.alphaBlendOnName);
                        material.DisableKeyword(BaseStyles.alphaPremultiplyOnName);
                        material.renderQueue = (renderQueueOverride >= 0) ? renderQueueOverride : (int)RenderQueue.Transparent;
                    }
                    break;

                case RenderingMode.Custom:
                    {
                        material.SetOverrideTag(BaseStyles.renderTypeName, BaseStyles.customRenderingModeNames[(int)customMode]);
                        // _SrcBlend, _DstBlend, _BlendOp, _ZTest, _ZWrite, _ColorWriteMask are controlled by UI.

                        switch (customMode)
                        {
                            case CustomRenderingMode.Opaque:
                                {
                                    material.DisableKeyword(BaseStyles.alphaTestOnName);
                                    material.DisableKeyword(BaseStyles.alphaBlendOnName);
                                    material.SetInt(BaseStyles.alphaToMaskName, (int)DepthWrite.Off);
                                }
                                break;

                            case CustomRenderingMode.Cutout:
                                {
                                    material.EnableKeyword(BaseStyles.alphaTestOnName);
                                    material.DisableKeyword(BaseStyles.alphaBlendOnName);
                                    material.SetInt(BaseStyles.alphaToMaskName, (int)DepthWrite.On);
                                }
                                break;

                            case CustomRenderingMode.Fade:
                                {
                                    material.DisableKeyword(BaseStyles.alphaTestOnName);
                                    material.EnableKeyword(BaseStyles.alphaBlendOnName);
                                    material.SetInt(BaseStyles.alphaToMaskName, (int)DepthWrite.Off);
                                }
                                break;
                        }

                        material.renderQueue = (renderQueueOverride >= 0) ? renderQueueOverride : material.renderQueue;
                    }
                    break;
            }

            // If Stencil is set to NotEqual, raise the queue by 1.
            if (material.GetInt("_StencilComp") == (int)CompareFunction.NotEqual)
            {
                material.renderQueue += 1;
            }
        }

		protected void FooterOptions()
		{
			EditorGUILayout.Space();

			if (WithChangeCheck(() => 
			{
				editor.ShaderProperty(renderQueueOverride, Content(BaseStyles.renderQueueOverrideName));
			})) {
				MaterialChanged(target);
			}

			// Show the RenderQueueField but do not allow users to directly manipulate it. That is done via the renderQueueOverride.
			GUI.enabled = false;
			editor.RenderQueueField();

			if (!GUI.enabled && !target.enableInstancing)
			{
				target.enableInstancing = true;
			}

			editor.EnableInstancingField();
		}
    }
}
